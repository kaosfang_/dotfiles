# KaosFang's .bashrc
export PATH="$PATH:'pwd'/flutter/bin"
# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# This allows will auto cd into a directory
#oshopt -s autocd

# This will let the shell insult you when you mistakes
# Just a reminder that you are a useless human being wasting oxygen
if [ -f /etc/bash.command-not-found ]; then
     . /etc/bash.command-not-found
fi

# Activate vi mode with <Escape>:
set -o vi

# User specific aliases and functions
alias cl="clear"
alias sdn="shutdown -h now"
alias upgrade="sudo dnf upgrade"
alias hackerman="cmatrix"
alias myip="curl ipinfo.io/ip"
alias v="vim"
alias mkd="mkdir -pv"
alias r="ranger"
alias www="lynx"
alias hh="htop"

# Terminal "bling"
neofetch


